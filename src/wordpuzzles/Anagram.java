package wordpuzzles;

import java.util.Vector;

class Anagram {

	static void anagram() {
		//Intro and collect input
		System.out.println("\nThis app will determine all anagrams of a word.\n");
		WordPuzzles.Sleep(1000);
		String input = Scrabble.getValidInput();
		char[] letters = Scrabble.sanitizeToArray(input);
		WordPuzzles.Sleep(700);
		
		//Find words and remove user's entry if found
		Vector<String> wordsList = Scrabble.findWords(letters, false, letters.length, ' ');
		wordsList.remove(input.toLowerCase());

		//Output results
		System.out.println();
		if (wordsList.isEmpty()) {
			System.out.println("There are no words found.");
			return;
		}
		System.out.println("Anagrams that can be made with those letters: ");
		for (int i=0; i<wordsList.size(); i++) {
			System.out.println(wordsList.get(i));
		}
		return;
	}
}
