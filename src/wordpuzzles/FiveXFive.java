package wordpuzzles;

import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Vector;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Hashtable;

class FiveXFive {

	private static final String dictFile = "/usr/share/dict/words";
		
	static void fiveX() {
		//Intro and get necessary info
		System.out.println("\nThis app provides a solution to a 5x5 word grid.\n");
		WordPuzzles.Sleep(1000);
		char[][] grid = new char[5][5];
		initGrid(grid);		
		WordPuzzles.Sleep(1000);
		
		//Read back and confirm
		System.out.println("\n\nOkay, here is what I have to start with:\n");
		boolean ready = false;
		while (!ready) {
			printGrid(grid);
			System.out.println("Is this correct? [Y/n]");
			if (WordPuzzles.yesNo()) {
				ready = true;
			} else {
				initGrid(grid);
			}
		}

		//Algorithm to find words to fit
		if(!solveGrid(grid)) {
			System.out.println("No solution could be found to that grid.");
		}
		return;
	}
	
	//Collects user's character set and validates it
	private static void initGrid(char[][] grid) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter your grid, one row at a time, using spaces for empty squares: ");
		for (int row=0; row<5; row++) {
			while (true) {
				try {
					int line = row + 1;
					System.out.print("    [row " + line + "]:  ");
					String input = scan.nextLine();
					//remove all other chars, returning just the letters and spaces
					input = input.replaceAll("[^a-zA-Z ]", "");
					//check that the input contained exactly characters
					if (input.matches("^[a-zA-Z ]{5}$")) {
						putInGrid(grid, row, input);
						break;
					} else {
						System.out.println("\nError, you must input exactly 5 characters per row. Please try again.");
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	//Print out grid with some formatting
	private static void printGrid(char[][] grid) {
		System.out.println("\n┌───┬───┬───┬───┬───┐");
		for (int row=0; row<5; row++) {
			for (int col=0; col<5; col++) {
				System.out.print("| " + grid[row][col] + " ");
			}
			if (row != 4) {
				System.out.println("|\n├───┼───┼───┼───┼───┤");
			}
		}
		System.out.println("|\n└───┴───┴───┴───┴───┘");
	}
	
	private static boolean solveGrid(char[][] grid) {
		Vector<Vector<String>> foundWords = new Vector<Vector<String>>(10);
		foundWords.setSize(10); //this ensures we don't end up with an out of bounds exception if we set foundWords in a strange order
		int progCheck = spaceCount(grid);
		//Start by grabbing words that would fit in the row or column, and committing it to the grid if only one result
		//Grid index "grindex" use: 0-4 are rows 1-5; 5-9 are columns 1-5 (i.e. row index lines up, but col index is off by 5)
		for (int grindex = 0; grindex < 10; grindex++) {
			String gridWord = getGridWord(grid, grindex);
			//TODO: handle the case where there are no gridWords with at least 2 letters (probably fail and say not enough info)
			if (gridWord.matches(".*[A-Z].*[A-Z].*")) { //if the grid word doesn't have at least 2 chars assigned, don't grab words
				Vector<String> temp = findWords(gridWord);
				if (temp.isEmpty()) {
					return false; //no solution if word could not be made with those letters. 
					//This also serves as a double check that a word that may have been formed in the previous recursive pass is actually a word
					//TODO: could improve performance here by not re-checking words that were already checked and set...
				} else if (temp.size() == 1) {
					putInGrid(grid, grindex, temp.get(0)); //If exactly one solution, just put it in
				}
				foundWords.set(grindex, temp);
			}			
		}
		//If the grid is filled out and we just passed through the word checks above, then we are successful
		if (spaceCount(grid) == 0) {
			printGrid(grid);
			return true;
		}
		//If the wordchecks made any progress then go recurse in with the same grid
		if (progCheck > spaceCount(grid)) {
			return solveGrid(grid);
		} else { //else if no progress was made, we need to take a guess and try a word in the grid (using a copy and recursion)
			//This is the difficult tricky bit. 
			//We should test using the shortest grindex vector, because if there is no solution for one gridWord, then the whole grid is not solvable
			//So only if we get a false return on all words in that vector do we end up with a failure here. 
			int min = 1;
			int ind = 0;  //initializing min and ind with grindex 0 so i can skip it in the loop:
			for (int i=0; i<foundWords.size(); i++) {
				try {
					int sz = foundWords.get(i).size();					
					if (min == 1 || (sz < min && sz > 1)) { //ensuring we don't end up with a vector of size 1, since we already dealt with those
						min = sz;
						ind = i;
					}
				} catch (NullPointerException e) { //since the vectors inside of the foundWords vector are not initialized, they may be null
					continue; //if that vector isn't set, then just continue to the next one
				}
			}
			//if min is still 1, then we don't have any grindex with more than two characters to attempt a solution with
			//we could go forth and solve with any 5 letter words that would fit, but that becomes a trivial solution so lets bail instead:
			if (min == 1) {
				System.out.println("Too many possiblilties, please include more initial letters");
				return false;
			}
			for (int i=min-1; i>=0; i--) { //starting at the end to de-prioritize the capital words, and give a boost to z and x words ;)
				char[][] gridCopy = copyGrid(grid); //Don't test words in the actual grid, use a clone
				putInGrid(gridCopy, ind, foundWords.get(ind).get(i)); //put one word from the vector into the appropriate position
				boolean bool = solveGrid(gridCopy); //Run it through
				if (bool) {
					//Awesome, that guess resulted in a solution. We should have already printed it, so lets just return out.
					return true;
				} //if bool is false, but we are not on last word in vector, then continue trying words..
			}
			//if we tried all the words in the vector and never got a true result, only failures, then we are done, there is no solution
			return false;
		}
	}
	
	private static int spaceCount(char[][] grid) {
		int count = 0;
		for (int i=0; i<5; i++) {
			for (int j=0; j<5; j++) {
				if (grid[i][j] == ' ') count++;
			}
		}
		return count;
	}
	
	private static String getGridWord(char[][] grid, int grindex) {
		//TODO: this would be better as a private get function for a grid class
		if (grindex < 5) {
			return String.valueOf(grid[grindex]); //String is that grid row
		} else {
			char[] tempArr = new char[5];
			for (int i=0; i<5; i++) {
				tempArr[i] = grid[i][grindex-5]; //For columns, need to grab char piecewise down 
			}
			return String.valueOf(tempArr);
		}
	}
	
	private static void putInGrid(char[][] grid, int grindex, String insert) {
		//TODO: this would be better as a private setter function to a grid class
		char[] tempArr = insert.toUpperCase().toCharArray();
		if (grindex < 5) {
			grid[grindex] = tempArr;
		} else {
			for (int i=0; i<5; i++) {
				grid[i][grindex-5] = tempArr[i];
			}
		}
	}

	//Turns out clone() does a shallow copy of the 2d array object, so I need to make a deep copy myself
	//I also thought i needed this multiple times, but I really only used it once, so maybe a separate method wasn't necessary
	private static char[][] copyGrid(char[][] grid) {
		char[][] newGrid = new char[5][5];
		for (int i=0; i<5; i++) {
			for (int j=0; j<5; j++) {
				newGrid[i][j] = grid[i][j];
			}
		}
		return newGrid;
	}
	
	/* ORIGINAL SOLUTION ATTEMPT. VERY BAD.
	private static boolean resolveGrid(char[][] grid) {
		int setWords = 0;
		int foundWords = 0;
		int iterations = 0;
		//Create hashtable to track which actual words can fit, and which grid rows/cols have been checked
		//Hash 1-5 is 'across' & hash 6-10 is 'down'
		Hashtable<Integer, Vector<String>> words = new Hashtable<Integer, Vector<String>>(10);

		while (setWords < 10) {
			iterations++;
			int foundWordsInit = foundWords;
			int setWordsInit = setWords;
			//start with the rows / across	
			for (int row=0; row<5; row++) {
				//if row already full, move on to next, but make sure its been added to "words" and setWords count
				if (String.valueOf(grid[row]).matches("^[A-Z]{5}$")) {
					if (!words.containsKey(row+1)) {
						//This should really never happen unless the grid is staged to be blank in a row one iteration and filled the next..
						//but we need to confirm that this word is acceptable now..
						//so let it move into next if statement
					} else if (words.get(row+1).size() == 1) {
						//only happens if we already setWord below
						continue;
					} else {
						//need to make sure grid now has a word that was already found as acceptable
						Vector<String> testWord = words.get(row+1);
						for (int i=0; i<testWord.size(); i++) {
							if (testWord.get(i).toUpperCase().equals(String.valueOf(grid[row]))) {
								testWord.clear();
								testWord.add(String.valueOf(grid[row]));
								words.put(row+1, testWord);
								setWords++;
								break;
							}
							//completed loop through vector and didn't find word match - no solution
							if (i == testWord.size()-1) return false;
						}
						continue;
					}
				}
				//if row contains at least 2 letters in the grid, fill out the vector for possible fits
				if (String.valueOf(grid[row]).matches(".*[A-Z].*[A-Z].*")) {
					Vector<String> temp = findWords(grid[row]);
					//if there were no words that match the set chars in the grid then there is no solution
					if (temp.isEmpty()) {
						return false;
					//if we get a single word, then that has to be the solution we continue with, set it in the grid
					} else if (temp.size() == 1) {
						grid[row] = temp.get(0).toUpperCase().toCharArray();
						setWords++;
						//if words for this key was already set, don't add to foundWords
						if (!words.containsKey(row+1)) foundWords++;
						words.put(row+1, temp);
					} else {
						//if words for this key was already set, don't add to foundWords
						if (!words.containsKey(row+1)) foundWords++;
						words.put(row+1, temp);
					}
				}
			}
			
			//now the columns / down (a little more complicated because I need to grab and set the chars from the 2D array by loop)
			for (int col=0; col<5; col++) {
				char[] downArr = new char[5];
				for (int i=0; i<5; i++) {
					downArr[i] = grid[i][col];
				}
				//if col already full, move on to next, but make sure its been added to "words" and setWords count
				if (String.valueOf(downArr).matches("^[A-Z]{5}$")) {
					if (!words.containsKey(col+6)) {
						//This should really never happen unless the grid is staged to be blank in a col one iteration and filled the next..
						//but we need to confirm that this word is acceptable now..
						//so let it move into next if statement
					} else if (words.get(col+6).size() == 1) {
						//only happens if we already setWord below
						continue;
					} else {
						//need to make sure grid now has a word that was already found as acceptable
						Vector<String> testWord = words.get(col+6);
						for (int i=0; i<testWord.size(); i++) {
							if (testWord.get(i).toUpperCase().equals(String.valueOf(downArr))) {
								testWord.clear();
								testWord.add(String.valueOf(downArr));
								words.put(col+6, testWord);
								setWords++;
								break;
							}
							//completed loop through vector and didn't find word match - no solution
							if (i == testWord.size()-1) return false;
						}
						continue;
					}
				}
				//if col contains at least 2 letters in the grid, fill out the vector for possible fits (same algorithm as rows above)
				if (String.valueOf(downArr).matches(".*[A-Z].*[A-Z].*")) {
					Vector<String> temp = findWords(downArr);
					if (temp.isEmpty()) {
						return false;
					} else if (temp.size() == 1) {
						for (int i=0; i<5; i++) {
							grid[i][col] = temp.get(0).toUpperCase().charAt(i);
						}
						setWords++;
						//if words for this key was already set, don't add to foundWords
						if (!words.containsKey(col+6)) foundWords++;
						words.put(col+6, temp);
					} else {
						//if words for this key was already set, don't add to foundWords
						if (!words.containsKey(col+6)) foundWords++;
						words.put(col+6, temp);
					}
				}
			}
			//If we didn't increase  foundwords or setWords, then iterating through is no longer a productive method to a solution
			if (foundWordsInit == foundWords && setWordsInit == setWords) break;
		}
		System.out.println("Iterations: " + iterations);
		//Check if we are done after these iterations
		if (setWords == 10) return true;
		
		//At this point we don't have the grid filled, so we take a guess at a word from the foundWords
		
		//start by finding the shortest vector, then loop that one into the grid solver
		int min = 0;
		int minInd = 0;
		int[] sizes = new int[10];
		for (int i=0; i<10; i++) {
			if (words.containsKey(i+1)) {
				sizes[i] = words.get(i+1).size();
			}
		}
		for (int i=0; i<sizes.length; i++) {
			if (sizes[i] == 0) continue;
			if (min == 0 || sizes[i] < min) {
				min = sizes[i];
				minInd = i+1;
			}
		}
		for (int i=0; i<words.get(minInd).size(); i++) {
			//need to feed the word into the grid differently if row or column
			char[][] gridCopy = grid.clone();
			char[] testWord = words.get(minInd).get(i).toUpperCase().toCharArray();
			if (minInd < 6) {
				gridCopy[minInd-1] = testWord;
			} else {
				for (int x=0; x<5; x++) {
					gridCopy[x][minInd-6] = testWord[x];
				}
			}
			if (!solveGrid(gridCopy)) {
				if (i == words.get(minInd).size()-1) return false;
				continue;
			}
			grid = gridCopy.clone();
			setWords++;
		}
		
		
		return true;
	}
*/

	private static Vector<String> findWords(String gridWord) {
		Vector<String> results = new Vector<String>();
		String word;
		try {
			FileReader fileReader = new FileReader(dictFile);
			BufferedReader bufferReader = new BufferedReader(fileReader);
			while ((word = bufferReader.readLine()) != null) {
				if (word.length() != 5) {
					continue;
				} else {
					//replace the spaces with a "." for use as wildcard in regex
					String test = gridWord.replaceAll(" ", ".");
					if (word.toUpperCase().matches(test)) {
						results.add(word);
					}
				}
			}
			bufferReader.close();
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file '" + dictFile + "'");
		} catch (IOException ex) {
			System.out.println("Error reading file '" + dictFile + "'");
		}
		return results;
	}
}
