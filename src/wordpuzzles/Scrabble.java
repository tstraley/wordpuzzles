package wordpuzzles;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.io.*;
import java.util.Vector;

class Scrabble {
	
	private static final String dictFile = "/usr/share/dict/words";
	
	//Main word building app function
	static void buildWords() {
		//Intro and get necessary info
		System.out.println("\nThis app will build words from sets of letters.\n");
		WordPuzzles.Sleep(1000);
		String input = getValidInput();
		char[] letters = sanitizeToArray(input);
	
		//Read back to them:
		readLetters(letters);
		WordPuzzles.Sleep(1000);
		
		//Are any of these chars required
		char mustContain = ' ';
		System.out.println("\nAre any of these letters required?");
		if (WordPuzzles.yesNo()) {
			System.out.println("Which letter is required?");
			Scanner scan = new Scanner(System.in);
			while (true) {
				try {
					input = scan.nextLine();
					input = input.toUpperCase();
					mustContain = input.charAt(0);
					if (Arrays.binarySearch(letters, mustContain) >= 0 ) {
						break;
					} else {
						System.out.println("'" + mustContain + "' is not in your list of letters. Please try again.");
						System.out.println("Which of your letters is required?");
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
				}
			}
		}
	
		//Are capitalized words allowed?
		boolean capsAllowed = false;
		System.out.println("\nAre capitalized words allowed?");
		if (WordPuzzles.yesNo()) {
			capsAllowed = true;
		}
		
		//Does the word need to be a certain length?
		int lengthReq = -1;
		System.out.println("Does the word have a required length?");
		if (WordPuzzles.yesNo()) {
			System.out.println("How many letters?");
			Scanner scan = new Scanner(System.in);
			while (true) {
				try {
					lengthReq = scan.nextInt();
					if (lengthReq > 0) {
						if (lengthReq <= letters.length) {
							break;
						} else {
							System.out.println("The word cannot be longer than the number of letters you supplied.");
							System.out.println("Please enter a smaller number of letters:");
						}
					} else {
						System.out.println("Please enter a positive integer only.");
						System.out.println("How many letters are required?");
					}
				} catch (InputMismatchException e) {
					scan.nextLine();
					System.out.println("Invalid input. Positive integers only please.");
				}
			}
		}
		
		//Parse through dictionary to find words
		Vector<String> wordsList = findWords(letters, capsAllowed, lengthReq, mustContain);
		
		//Output results
		System.out.println();
		if (wordsList.isEmpty()) {
			System.out.println("There are no words to be made with those requirements.");
			return;
		}
		System.out.println("Words that can be made: ");
		for (int i=0; i<wordsList.size(); i++) {
			System.out.println(wordsList.get(i));
		}
		return;
	}
	
	//Collects user's character set and validates it
	static String getValidInput() {
		Scanner scan = new Scanner(System.in);
		while (true) {
			System.out.println("Please enter your character set: ");
			try {
				String input = scan.nextLine();
				//check that the input contained at least 2 alpha chars somewhere within
				if (input.matches(".*[a-zA-Z].*[a-zA-Z].*")) {
					//remove all other chars, returning just the letters
					return input.replaceAll("[^a-zA-Z]", "");
				} else {
					System.out.println("\n\nSorry, you must input at least 2 letters. Please try again.");
				}
			} catch (NoSuchElementException e) {
				e.printStackTrace();
			}
		}
	}
	
	//Sanitizes the string of letters and returns it in an array of characters to use
	static char[] sanitizeToArray(String input) {
		input = input.toUpperCase();
		char[] chars = input.toCharArray();
		Arrays.sort(chars);
		return chars;
	}
	
	static void readLetters(char[] letters) {
		System.out.println("Okay, great! Lets see what we can create from the letters:");
		for (int i=0; i<letters.length; i++) {
			System.out.print(letters[i] + ", ");
			if (i==letters.length-2) {
				System.out.print("and ");
			}
		}
	}
	
	//Return list of words that can be made with the letters and requirements 
	static Vector<String> findWords(char[] letters, boolean capsAllowed, int lengthReq, char mustContain) {
		String word;
		Vector<String> results = new Vector<String>();
		try {
			FileReader fileReader = new FileReader(dictFile);
			BufferedReader bufferReader = new BufferedReader(fileReader);
			if (lengthReq == -1 && mustContain == ' ') {
				while ((word = bufferReader.readLine()) != null) {
					if (!capsAllowed && word.matches("^[A-Z].*")) {
						continue;
					} else if (testWord(letters, word)) {
						results.add(word);
					}
				}
				bufferReader.close();
			} else if (lengthReq == -1 && mustContain != ' ') {
				while ((word = bufferReader.readLine()) != null) {
					if (!capsAllowed && word.matches("^[A-Z].*")) {
						continue;
					} else if (word.length() > letters.length || word.length() == 1 || word.toUpperCase().indexOf(mustContain) == -1){
						continue;
					} else if (testWord(letters, word)) {
						results.add(word);
					}
				}
				bufferReader.close();
			} else if (lengthReq > 0 && mustContain == ' ') {
				while ((word = bufferReader.readLine()) != null) {
					if (word.length() != lengthReq) {
						continue;
					} else if (!capsAllowed && word.matches("^[A-Z].*")) {
						continue;
					} else if (testWord(letters, word)) {
						results.add(word);
					}
				}
				bufferReader.close();
			} else if (lengthReq > 0 && mustContain != ' ') {
				while ((word = bufferReader.readLine()) != null) {
					if (word.length() != lengthReq || word.toUpperCase().indexOf(mustContain) == -1) {
						continue;
					}else if (!capsAllowed && word.matches("^[A-Z].*")) {
						continue;
					} else if (testWord(letters, word)) {
						results.add(word);
					}
				}
				bufferReader.close();
			} else System.out.println("Error: Inappropriate use of Word Finder.");
		}
		catch (FileNotFoundException ex) {
			System.out.println("Unable to open file '" + dictFile + "'");
		}
		catch (IOException ex) {
			System.out.println("Error reading file '" + dictFile + "'");
		}
		results.trimToSize();
		return results;
	}
	
	private static boolean testWord(char[] letters, String word) {
		if (word.length() > letters.length || word.length() == 1) {
			return false;
		} else if (word.length() == letters.length) {
			char[] test = sanitizeToArray(word);
			if (Arrays.equals(test, letters)) {
				return true;
			} else {
				return false;
			}
		} else {
			char[] test = sanitizeToArray(word);
			char[] chars = Arrays.copyOf(letters, letters.length);
			return charCompareLoop(test, chars);
		}
	}
	
	static boolean charCompareLoop(char[] test, char[] chars) {
		for (int i=0; i<test.length; i++) {
			for (int x=0; x<chars.length; x++) {
				if (test[i]==chars[x]) {
					chars[x]=' ';
					break;
				} else if (x==chars.length-1) {
					return false;
				}
			}				
		}
		return true;
	}
	
}
