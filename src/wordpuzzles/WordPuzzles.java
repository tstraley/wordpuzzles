package wordpuzzles;
import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;

class WordPuzzles {

	public static void main(String[] args) {
		// Welcome and initial menu prompt
		System.out.println("Hello! Welcome to WordPuzzles, where we help you solve a variety of word games.\n");
		Sleep(500);
		System.out.println("Which word game would you like help with?");
		Sleep(500);
		System.out.println("    [Press 1] Scrabble / Word Builders  [Press 1]");
		System.out.println("    [Press 2] Nineagram  [Press 2]");
		System.out.println("    [Press 3] 5X5  [Press 3]");
		System.out.println("    [Press 4] Anagrams  [Press 4]");
		
		//Collect user choice and take to appropriate game
		try (final Scanner scan = new Scanner(System.in)) {
			boolean validInput = false;
			while (!validInput) {
				try {
					int input = scan.nextInt();
					switch (input) {
						case 1:
							validInput = true;
							Scrabble.buildWords();
							break;
						case 2:
							validInput = true;
							Nineagram.nineagram();
							break;
						case 3:
							validInput = true;
							FiveXFive.fiveX();
							break;
						case 4:
							validInput = true;
							Anagram.anagram();
							break;
						default:
							System.out.println("Sorry, but I couldn't find an option for '" + input + "'. Please try again.");
					}
				} catch (InputMismatchException e) {
					String input = scan.nextLine();
					System.out.println("Error: Input '" + input + "' is not an available option. Try again.");
				}
			}
		}
		return;
	}

	static void Sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	static boolean yesNo() {
		Scanner scan = new Scanner(System.in);
		while (true) {
			try {
				String input = scan.nextLine();
				input = input.toLowerCase();
				if (input.equals("yes") || input.equals("y")) {
					return true;
				} else if (input.equals("no") || input.equals("n")) {
					return false;
				} else {
					System.out.println("\n\nInvalid response. Please use 'Yes'/'No' only.");
				}
			} catch (NoSuchElementException e) {
				e.printStackTrace();
			}
		}
	}
}
