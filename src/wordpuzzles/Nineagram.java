package wordpuzzles;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Vector;

class Nineagram {
	
	static void nineagram() {
		//Intro and get necessary info
		System.out.println("\nThis app provides solutions to a nineagram cross.\n");
		WordPuzzles.Sleep(1000);
		String input = getValidInput();
		char[] letters = Scrabble.sanitizeToArray(input);
	
		//Read back to them:
		Scrabble.readLetters(letters);
		WordPuzzles.Sleep(1000);

		//Start with creating a list of all possible 5 letter words that can be made with the letters
		Vector<String> wordsList = Scrabble.findWords(letters, false, 5, ' ');
		
		//Now look through these words for ones that meet in the middle and don't overuse letters
		System.out.println("\n");
		if (wordsList.size() < 2) {
			System.out.println("There are no words to be made with those letters.");
			return;
		}
		boolean foundSoln = false;
		for (int ind1=0; ind1<wordsList.size()-1; ind1++) {
			for (int ind2=ind1+1; ind2<wordsList.size(); ind2++) {
				//Center letter (index 2) must be the same
				String word1 = wordsList.get(ind1);
				String word2 = wordsList.get(ind2);
				if (word1.charAt(2) ==  word2.charAt(2)) {
					//Ensure letters (other than center) are not used more than once
					char[] test = Scrabble.sanitizeToArray(word1 + word2);
					char[] chars = Arrays.copyOf(letters, letters.length+1);
					chars[9] =  Character.toUpperCase(word1.charAt(2));
					if (Scrabble.charCompareLoop(test, chars)) {
						System.out.println(word1 + " and " + word2);
						foundSoln = true;
					}
				}
			}
		}
		if (!foundSoln) {
			System.out.println("There are no solutions to ninegram using those letters.");
		}
		return;
	}
	
	//Collects user's character set and validates it
	private static String getValidInput() {
		Scanner scan = new Scanner(System.in);
		while (true) {
			System.out.println("Please enter your character set: ");
			try {
				String input = scan.nextLine();
				//remove all other chars, returning just the letters
				input = input.replaceAll("[^a-zA-Z]", "");
				//check that the input contained exactly 9 alpha chars somewhere within
				if (input.matches("^[a-zA-Z]{9}$")) {
					return input;
				} else {
					System.out.println("\n\nSorry, you must input exactly 9 letters for nineagram. Please try again.");
				}
			} catch (NoSuchElementException e) {
				e.printStackTrace();
			}
		}
	}
	
}
