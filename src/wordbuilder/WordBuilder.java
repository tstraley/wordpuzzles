package wordbuilder;
import java.util.Arrays;
import java.util.Scanner;
import java.io.*;
import java.util.Vector;

public class WordBuilder {

	public static void main(String[] args) {
		//Welcome Prompt and Character Collection!
		System.out.println("Hello! Welcome to WordBuilder!");
		System.out.println("Please enter your character set: ");
		Scanner user = new Scanner(System.in);
		String input = user.nextLine();
		char letters[] = inputValidation(input);
		//Read back to them:
		System.out.println("Okay, great! Lets see what words we can create from the letters:");
		for (int i=0; i<letters.length; i++) {
			System.out.print(letters[i]);
			System.out.print(", ");
			if (i==letters.length-2) {
				System.out.print("and ");
			}
		if (user!=null)
			user.close();
		}
		//Parse through dictionary to find words
		Vector<String> wordsList = findWords(letters);
		//Output results
		System.out.println();
		if (wordsList.isEmpty()) {
			System.out.println("There are no words to be made with that set of letters.");
			return;
		}
		System.out.println("Words that can be made: ");
		for (int i=0; i<wordsList.size(); i++) {
			System.out.println(wordsList.get(i));
		}
		return;
	}

	private static char[] inputValidation(String input) {
		//Validate that there are letters in the input, parse out non-letter chars, make all upper-case, sort, and place into a char array
		while (true) {
			if (input.matches(".*[a-zA-Z].*")) {
				break;
			} else {
				System.out.println();
				System.out.println("Sorry, but I could not find any letters to use. Please try again to enter the character set.");
				System.out.print("Characters set: ");
				Scanner user = new Scanner(System.in);
				input = user.nextLine();
			}
		}
		input = input.replaceAll("[^a-zA-Z]", "");
		input = input.toUpperCase();
		char[] letters = input.toCharArray();
		Arrays.sort(letters);
		return letters;
	}
	
	private static Vector<String> findWords(char[] letters) {
		String fileName = "/usr/share/dict/words";
		String word;
		Vector<String> results = new Vector<String>();
		try {
			FileReader fileReader = new FileReader(fileName);
			BufferedReader bufferReader = new BufferedReader(fileReader);
			while ((word = bufferReader.readLine()) != null) {
				if (testWord(letters, word)) {
					results.add(word);
				}
			}
			bufferReader.close();
		}
		catch (FileNotFoundException ex) {
			System.out.println("Unable to open file '" + fileName + "'");
		}
		catch (IOException ex) {
			System.out.println("Error reading file '" + fileName + "'");
		}
		results.trimToSize();
		return results;
	}
	
	private static boolean testWord(char[] letters, String word) {
		if (word.length() > letters.length) {
			return false;
		} else if (word.length() == letters.length) {
			String temp = word;
			temp = temp.toUpperCase();
			char[] test = temp.toCharArray();
			Arrays.sort(test);
			if (Arrays.equals(test, letters)) {
				return true;
			} else {
				return false;
			}
		} else {
			String temp = word;
			temp = temp.toUpperCase();
			char[] test = temp.toCharArray();
			char[] chars = Arrays.copyOf(letters, letters.length);
			for (int i=0; i<test.length; i++) {
				for (int x=0; x<chars.length; x++) {
					if (test[i]==chars[x]) {
						chars[x]=' ';
						break;
					} else if (x==chars.length-1) {
						return false;
					}
				}				
			}
		}
		return true;
	}
}
