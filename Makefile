PROJECT ?= WordPuzzles
_prj := $(shell echo $(PROJECT) | tr A-Z a-z)

build:
	@echo "==> Building ${PROJECT}"
	@mkdir -p bin
	javac -d bin ./src/$(_prj)/*.java

clean:
	rm -rf bin

exec:
	java -cp bin $(_prj).$(PROJECT)


.PHONY: build clean exec