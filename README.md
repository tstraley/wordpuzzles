# Java WordPuzzles

This was my first project in Java. Living in New Zealand at the time,
I was interested in creating a tool that could let me check my answers
to various newspaper word puzzle games that were common in the publication
I received. Those papers would otherwise make you wait until the next
release to see the answers, and I'm rather impatient.

## Disclaimer

The source code here was put together while I was a novice programmer and
had zero Java experience. I haven't looked over this code in years and no
longer touch Java so this is just here to maybe look back on way down the
road when I want to learn another new language.

## WordBuilder

This is just a simple program to give you possible words from a set of 
given letters. Interact via command line prompts.

### Compile

```shell
make build PROJECT=WordBuilder
```

### Run

```shell
java -cp bin wordbuilder.WordBuilder
```

Or alternatively:
```shell
make exec PROJECT=WordBuilder
```

## WordPuzzles

This command-line program is capable of solving several different 
word-based puzzles. Interact via prompts on the cli.

### Compile

```shell
make build
```

### Run

```shell
make exec
```

## Tips

If you don't have the java sdk installed to compile this program
you can do so using the `openjdk:7` docker image instead, eg.:

```shell
mkdir bin
docker run --rm -u "$(id -u):$(id -g)" -v "$PWD":/mnt -w /mnt openjdk:7 javac -d bin ./src/wordpuzzles/*.java
